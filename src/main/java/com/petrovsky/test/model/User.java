package com.petrovsky.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Users")
public class User extends BaseEntity {
    public static final String NAME_JSON = "nm",
                               PASSWORD_JSON = "psw",
                               ROLE_JSON = "role",
                               BOOKS_JSON = "books";

    public enum Role {
        ADMIN, USER, DEFAULT
    }

    @SerializedName(NAME_JSON)
    private String name;

    @SerializedName(PASSWORD_JSON)
    private String password;

    @Enumerated(EnumType.STRING)
    @SerializedName(ROLE_JSON)
    private Role role;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @SerializedName(BOOKS_JSON)
    private List<Book> books;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "User{id=" + super.getId() +
                ", name='" + name + '\'' +
                ", role=" + role +
                ", books=" + books +
                '}';
    }
}
