package com.petrovsky.test.model;

import com.google.gson.annotations.SerializedName;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Books")
public class Book extends BaseEntity {
    public static final String NAME_JSON = "nm",
                               AUTHOR_JSON = "aut",
                               GENRE_JSON = "gnr",
                               DOWNLOAD_PATH = "dwn";


    @SerializedName(NAME_JSON)
    private String name;

    @SerializedName(AUTHOR_JSON)
    private String author;

    @SerializedName(GENRE_JSON)
    private String genre;

    @SerializedName(DOWNLOAD_PATH)
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Book{" + "" +
                "id=" + super.getId() + '\'' +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
