package com.petrovsky.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@MappedSuperclass
public class BaseEntity {
    public static final String ID_JSON = "id";


    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @SerializedName(ID_JSON)
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
