package com.petrovsky.test.repository;

import com.petrovsky.test.model.Book;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BookDao extends BaseDao<Book, Long> {
    private static final String BOOK_DELETE = "DELETE FROM Book b WHERE b.id = ?1",
                                BOOK_SEARCH_BY_NAME = "SELECT b FROM Book b WHERE b.name LIKE ?1",
                                BOOK_SEARCH_BY_AUTHOR = "SELECT b FROM Book b WHERE b.author LIKE ?1",
                                BOOK_SEARCH_BY_GENRE = "SELECT b FROM Book b WHERE b.genre LIKE ?1",
                                BOOK_GET_ALL = "SELECT b FROM Book b";

    @Transactional
    public Boolean delete (Long id) {
        return getEntityManager().createQuery(BOOK_DELETE).setParameter(1, id).executeUpdate() != 0;
    }

    public List<Book> searchByName (String name) {
        return getEntityManager().createQuery(BOOK_SEARCH_BY_NAME, getPersistentClass()).setParameter(1, "%" + name + "%").getResultList();
    }

    public List<Book> searchByAuthor (String author) {
        return getEntityManager().createQuery(BOOK_SEARCH_BY_AUTHOR, getPersistentClass()).setParameter(1, "%" + author + "%").getResultList();
    }

    public List<Book> searchByGenre (String genre) {
        return getEntityManager().createQuery(BOOK_SEARCH_BY_GENRE, getPersistentClass()).setParameter(1, "%" + genre + "%").getResultList();
    }

    public List<Book> getAll() {
        return getEntityManager().createQuery(BOOK_GET_ALL, getPersistentClass()).getResultList();
    }

}
