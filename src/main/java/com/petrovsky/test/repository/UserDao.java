package com.petrovsky.test.repository;

import com.petrovsky.test.model.Book;
import com.petrovsky.test.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDao extends BaseDao<User, Long>{
    private static final String USER_GET_BY_NAME = "SELECT u FROM User u WHERE u.name = ?1 AND u.password = ?2";

    public User getByNameAndPassword (String name, String password) {
        return getEntityManager()
                .createQuery(USER_GET_BY_NAME, getPersistentClass())
                .setParameter(1, name)
                .setParameter(2, password)
                .getSingleResult();
    }
}
