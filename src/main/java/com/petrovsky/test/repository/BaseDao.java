package com.petrovsky.test.repository;

import com.petrovsky.test.model.BaseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

@Repository
public abstract class BaseDao<T extends BaseEntity, ID extends Serializable> {

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> persistentClass;

    public BaseDao() {
        //noinspection unchecked
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    public T get(ID id) {
        return entityManager.find(getPersistentClass(), id);
    }

    @Transactional
    public void save(T entity) {
        if(entity.getId() == null) {
            entityManager.persist(entity);
        } else {
            entityManager.merge(entity);
        }
    }
}
