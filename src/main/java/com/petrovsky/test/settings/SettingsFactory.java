package com.petrovsky.test.settings;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class SettingsFactory {

    public static final String CONFIG_PATH = "config/server.properties",
                               DEFAULT_HOST_NAME = "localhost";
    public static final Integer DEFAULT_PORT_NUMBER = 8099;
    public static final String DEFAULT_FILES_PATH = "data/files/";

    private static ServerSettings settings;

    private SettingsFactory() {
    }

    public static ServerSettings getInstance() throws IOException {
        Properties properties = new Properties();
        settings = new ServerSettings();
        properties.load(FileUtils.openInputStream(new File(CONFIG_PATH)));
        settings.setHost(properties.getProperty("server.host"));
        settings.setPort(Integer.parseInt(properties.getProperty("server.port")));
        settings.setFilesPath(properties.getProperty("files.path"));
        checkSettings(settings);
        return settings;
    }

    private static void checkSettings(ServerSettings settings) {
        if (settings != null) {
            if (settings.getHost() == null) settings.setHost(DEFAULT_HOST_NAME);
            if (settings.getPort() == null) settings.setPort(DEFAULT_PORT_NUMBER);
            if (settings.getPort() == null) settings.setFilesPath(DEFAULT_FILES_PATH);
        }
    }
}
