package com.petrovsky.test.webapp.controller;

import com.petrovsky.test.model.Book;
import com.petrovsky.test.model.User;
import com.petrovsky.test.repository.BookDao;
import com.petrovsky.test.repository.UserDao;
import com.petrovsky.test.utils.JsonUtils;
import com.petrovsky.test.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.*;

@Controller
public class UserController {

    public static final String TOKEN_KEY = "accessToken";

    @Autowired
    private UserDao userDao;

    private Boolean isValid(Object object) {
        return object != null && (!(object instanceof String) || !((String) object).isEmpty());
    }

    @RequestMapping(value = "api/user/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String userLogin(HttpServletResponse response, @RequestBody String json) {
        String token;
        if(isValid(json)) {
            User sentUser = JsonUtils.fromJson(User.class, json);
            User existUser = userDao.getByNameAndPassword(sentUser.getName(), sentUser.getPassword());
            if (existUser != null) {
                token = TokenUtils.generateToken(existUser);
                Cookie cookie = new Cookie(TOKEN_KEY, token);
                cookie.setPath("/");
                response.addCookie(cookie);
                return JsonUtils.getJson(existUser);
            }
        }
        return null;
    }

    @RequestMapping(value = "api/user/registration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String toRegisterUser(@RequestBody String json) {
        if(isValid(json)) {
            User user = JsonUtils.fromJson(User.class, json);
            user.setRole(User.Role.USER);
            userDao.save(user);
            return "OK";
        }
        return "Error";
    }

    @RequestMapping(value = "api/user/get/current/role", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getUserRole(@CookieValue(value = TOKEN_KEY, required = false) String token) {
        User defaultUser = new User();
        defaultUser.setRole(User.Role.DEFAULT);

        if(isValid(token)) {
            User user = TokenUtils.parseToken(token);
            if (user.getRole() != null) {
                return JsonUtils.getJson(user);
            } else {
                return JsonUtils.getJson(defaultUser);
            }
        }
        return JsonUtils.getJson(defaultUser);
    }

    @RequestMapping(value = "api/user/get/current", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getCurrentUser(@CookieValue(value = TOKEN_KEY, required = false) String token) {
        if(isValid(token)) {
            Long id = TokenUtils.parseToken(token).getId();
            return JsonUtils.getJson(userDao.get(id));
        }
        return "Error";
    }

    @RequestMapping(value = "api/user/cart/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getUserRole(@CookieValue(value = TOKEN_KEY, required = false) String token, @RequestBody String json) {
        User user;
        Book book;
        if(isValid(token) && isValid(json)) {
            Long id = TokenUtils.parseToken(token).getId();
            user = userDao.get(id);
            book = JsonUtils.fromJson(Book.class, json);
            if(user.getBooks() != null) {
                user.getBooks().add(book);
                return "OK";
            } else user.setBooks(Collections.singletonList(book));
            userDao.save(user);
        }
        return "Error";
    }
}
