package com.petrovsky.test.webapp.controller;

import com.petrovsky.test.model.Book;
import com.petrovsky.test.repository.BookDao;
import com.petrovsky.test.repository.UserDao;
import com.petrovsky.test.settings.SettingsFactory;
import com.petrovsky.test.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

@Controller
@RequestMapping("api/book")
public class BookController {

    public static final int MAX_IMAGE_SIZE = 10 * 1024 * 1024;

    @Autowired
    private BookDao bookDao;

    private Boolean isValid(Object object) {
        return object != null && (!(object instanceof String) || !((String) object).isEmpty());
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getAll() {
        return JsonUtils.getJson(bookDao.getAll());
    }

    @RequestMapping(value = "/by/name", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String searchByName(@RequestBody String name) {
        if (isValid(name)) {
            return JsonUtils.getJson(bookDao.searchByName(name));
        } else return "Error";
    }

    @RequestMapping(value = "/by/author", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String searchByAuthor(@RequestBody String author) {
        if(isValid(author)) {
            return JsonUtils.getJson(bookDao.searchByAuthor(author));
        } else return "Error";

    }

    @RequestMapping(value = "/by/genre", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String searchByGenre(@RequestBody String genre) {
        if(isValid(genre)) {
            return JsonUtils.getJson(bookDao.searchByGenre(genre));
        } else return "Error";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String addBook(@RequestParam(value = "obj", required = false) String json, @RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
        if(isValid(json) && isValid(file)) {
            Book book = JsonUtils.fromJson(Book.class, json);
            book.setPath(saveFile(file));
            System.out.println(book.toString());
            bookDao.save(book);
            return "OK";
        }
        return "Error";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String deleteBook(@RequestBody String json) throws IOException {
        if(isValid(json)) {
            Book book = JsonUtils.fromJson(Book.class, json);
            bookDao.delete(book.getId());
            return "OK";
        }
        return "Error";
    }

    private String saveFile (MultipartFile file) throws IOException {
        OutputStream stream;
        if (file.getSize() > MAX_IMAGE_SIZE) return "Error: File is too large..";
        if (!file.getContentType().contains("application/pdf")) return "Error: File type is not pdf";

        byte[] bytes = file.getBytes();
        File fullPath = new File(SettingsFactory.getInstance().getFilesPath());
        if (!fullPath.exists()) fullPath.mkdirs();

        String fileName = file.getOriginalFilename();

        String fullName = StringUtils.cleanPath(fullPath.getPath()) + "/" + fileName;
        stream = new FileOutputStream(fullName);
        FileCopyUtils.copy(bytes, stream);
        stream.close();
        return fileName;
    }

}
