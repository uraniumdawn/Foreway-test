package com.petrovsky.test.webapp.interceptor;

import com.petrovsky.test.model.User;
import com.petrovsky.test.utils.TokenUtils;
import com.petrovsky.test.webapp.controller.UserController;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdminActionInterceptor extends HandlerInterceptorAdapter {

    private Boolean isValid(Object object) {
        return object != null && (!(object instanceof String) || !((String) object).isEmpty());
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        if(isValid(cookies)) {
            for (Cookie cookie: cookies) {
                return UserController.TOKEN_KEY.equals(cookie.getName()) && TokenUtils.parseToken(cookie.getValue()).getRole() == User.Role.ADMIN;
            }
        }
        return false;
    }
}
