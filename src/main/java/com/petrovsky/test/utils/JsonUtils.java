package com.petrovsky.test.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

public class JsonUtils {

	private static final GsonBuilder gb = new GsonBuilder()
			.serializeNulls()
			.setLongSerializationPolicy(LongSerializationPolicy.STRING)
			.disableHtmlEscaping();

	public static String getJson(Object object) {
		return gb.create().toJson(object);
	}

	public static <T extends Object> T fromJson(Class clazz, String json) {
		return (T) gb.create().fromJson(json, clazz);
	}
}
