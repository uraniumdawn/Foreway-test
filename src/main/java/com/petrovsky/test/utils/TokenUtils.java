package com.petrovsky.test.utils;


import com.petrovsky.test.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;

public class TokenUtils {

    @Value("${jwt.secret}")
    private static final String SECRET_CODE = "q235a43g4aDEg6FGdgdf45f4g44wf";

    public static String generateToken(User user) {
        Claims claims = Jwts.claims().setSubject(user.getName());
        claims.put("userId", String.valueOf(user.getId()));
        claims.put("role", user.getRole());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, SECRET_CODE)
                .compact();
    }

    public static User parseToken(String token) {
            Claims body = Jwts.parser()
                    .setSigningKey(SECRET_CODE)
                    .parseClaimsJws(token)
                    .getBody();

            User user = new User();
            user.setName(body.getSubject());
            user.setId(Long.parseLong(String.valueOf(body.get("userId"))));
            user.setRole(User.Role.valueOf((String)body.get("role")));

            return user;
    }
}
