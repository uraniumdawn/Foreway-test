app.config(function ($routeProvider, Roles) {
    $routeProvider
        .when('/',
            {
                controller: 'AppController',
                templateUrl: 'partials/main.html'
            })
        .when('/book/info',
            {
                controller: 'AppController',
                templateUrl: 'partials/book_info.html'
            })
        .when('/user/info',
            {
                resolve: {permission: function(AuthorizationService) {
                    return AuthorizationService.permission([Roles.USER], '/login');
                }
                },
                controller: 'AppController',
                templateUrl: 'partials/user_info.html'
            })
        .when('/login',
            {
                controller: 'AppController',
                templateUrl: 'partials/login.html'
            })
        .when('/registration',
            {
                controller: 'AppController',
                templateUrl: 'partials/registration.html'
            })
        .when('/admin/book/info',
            {
                resolve: {permission: function(AuthorizationService) {
                        return AuthorizationService.permission([Roles.ADMIN], '/book/info');
                    }
                },
                controller: 'AppController',
                templateUrl: 'partials/admin_book_info.html'
            })
        .when('/admin/book/add',
            {
                resolve: {permission: function(AuthorizationService) {
                    return AuthorizationService.permission([Roles.ADMIN], '/login');
                }
                },
                controller: 'AppController',
                templateUrl: 'partials/add_book.html'
            })
        .when('/admin/book/edit',
            {
                resolve: {permission: function(AuthorizationService) {
                        return AuthorizationService.permission([Roles.ADMIN], '/login');
                    }
                },
                controller: 'AppController',
                templateUrl: 'partials/admin_book_info.html'
            })
        .otherwise('404.html')
    });
