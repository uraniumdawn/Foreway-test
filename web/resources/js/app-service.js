app.factory('AppService', ['$http', function ($http) {
    var bookDTO = {};

    return {

        getAllBooks: function () {
            return $http.get('api/book/all');
        },
        searchByName: function (value) {
            return $http.post('api/book/by/name', value);
        },
        searchByAuthor: function (value) {
            return $http.post('api/book/by/author', value);
        },
        searchByGenre: function (value) {
            return $http.post('api/book/by/genre', value);
        },
        saveBook: function (value) {
            return $http.post('api/book/add', value, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            });
        },
        addToBookCart: function (value) {
            return $http.post('api/user/cart/add', value)
        },

        deleteBook: function (value) {
            return $http.post('api/book/delete', value);
        },
        setBook: function(value){
            bookDTO = value;
        },
        getBook: function(){
            return bookDTO;
        },
        logIn: function(value) {
            return $http.post('api/user/login', value);
        },
        signIn: function(value) {
            return $http.post('api/user/registration', value);
        },
        getCurrentUser: function() {
            return $http.get('api/user/get/current');
        }
    };
}]);
