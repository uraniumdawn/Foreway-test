app.controller('AppController', [
    '$scope',
    '$location',
    'AppService',
    'AuthorizationService',
    'Roles',
    function ($scope, $location, AppService, AuthorizationService, Roles) {
        console.log("Init AppController");

        $scope.searchedName = '';
        $scope.searchedAuthor = '';
        $scope.searchedGenre = '';
        $scope.book = {};
        $scope.currentUser = {};

        $scope.books = [];
        $scope.getAllBooks = function() {
            AppService.getAllBooks().then(function(promise) {
                $scope.books = promise.data;
            });
        };

        $scope.searchByName = function() {
            AppService.searchByName($scope.searchedName).then(function(promise) {
                $scope.books = promise.data;
            });
        };

        $scope.searchByAuthor = function() {
            AppService.searchByAuthor($scope.searchedAuthor).then(function(promise) {
                $scope.books = promise.data;
            });
        };

        $scope.searchByGenre = function() {
            AppService.searchByGenre($scope.searchedGenre).then(function(promise) {
                $scope.books = promise.data;
            });
        };

        $scope.setFile = function(file) {
            if(file) {
                $scope.fd = new FormData();
                $scope.fd.append('file', file[0]);
            }
        };

        $scope.saveBook = function() {
            AppService.setBook(null);
            if(!$scope.fd) {
                $scope.fd = new FormData();
            }
            $scope.fd.append('obj', JSON.stringify($scope.book));
            AppService.saveBook($scope.fd).then(function(promise) {
                $location.path('/');
            });
        };

        $scope.setBookForView = function(book) {
            AppService.setBook(book);
            $location.path('admin/book/info');
        };

        $scope.editBook = function() {
            $location.path('/admin/book/add');
        };

        $scope.deleteBook = function() {
            AppService.deleteBook($scope.book).then(function(promise) {
                $location.path('/');
            });
        };

        $scope.getBook = function() {
            $scope.book = AppService.getBook();
        };

        $scope.backToMain= function() {
            $location.path('/');
        };

        $scope.addToBookCart = function() {
            AppService.addToBookCart(AppService.getBook()).then(function(promise) {
                $location.path('/user/info');
            });
        };

        //--------------------------------------------------------------------------------------------------------------
        $scope.logIn = function() {
            AppService.logIn($scope.currentUser).then(function(promise){
                $location.path('/');
            });
        };

        $scope.signIn = function() {
            AppService.signIn($scope.currentUser).then(function(promise){
                $location.path('/');
            });
        };

        $scope.getCurrentUser = function() {
            AppService.getCurrentUser().then(function(promise) {
                $scope.currentUser = promise.data;
            });
        };

        $scope.getHeader = function() {
            AuthorizationService.getCurrentUser().then(function(promise){
                $scope.currentUser = promise.data;
                if(Roles.USER == $scope.currentUser.role) {
                    $scope.header = 'partials/logged_in_header.html';
                } else if (Roles.ADMIN == $scope.currentUser.role) {
                    $scope.header = 'partials/admin_header.html';
                } else {
                    $scope.header = 'partials/logged_out_header.html';
                }
            });
        }
    }
]);
