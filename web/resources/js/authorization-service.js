app.factory('AuthorizationService', function ($q, $http, $location) {

    return {

        permission: function (acceptableRoles, redirectPath) {
            var defer = $q.defer();
            var role = undefined;
            this.getCurrentUser().then(function(promise) {
                role = promise.data.role;
                if(contain(acceptableRoles, role)) {
                    defer.resolve();
                } else {
                    defer.reject();
                    $location.path(redirectPath)
                }
            });
            return defer.promise;
        },

        getCurrentUser: function() {
            return $http.get('api/user/get/current/role');
        }
    };
});

function contain(arr, val) {
    return arr.some(function(arrVal) {
        return val === arrVal;
    });
}
